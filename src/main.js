import { createApp } from 'vue'
import App from './App.vue'
import Characters from '../dist/characters/index.esm'

const app = createApp(App);

app.component('characters', Characters)
    .mount('#app')