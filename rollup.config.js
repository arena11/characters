import vue from 'rollup-plugin-vue'

export default [
  {
    input: 'src/components/Characters.vue',
    output: [
      {
        format: 'esm',
        file: 'dist/characters/index.esm.js'
      }
    ],
    plugins: [
      vue({
          template: {
              optimizeSSR: true
          }
      })
    ]
  }
]